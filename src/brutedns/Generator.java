package brutedns;

import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Radim Kolar
 */
public class Generator extends Configured implements Tool {

	final static char[] validchars = { 'a','b','c','d','e','f','g','h','i','j','k',
		'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
		'-','0','1','2','3','4','5','6','7','8','9',
	};

	/* (non-Javadoc)
	 * @see org.apache.hadoop.util.Tool#run(java.lang.String[])
	 */
	@Override
	public int run(String[] argv) throws Exception {
		String directory;
		String from;
		String to;
		int segments;
		SequenceFile.Writer writers[];
		int[] current;
		int[] stop;
		int active;
		BooleanWritable FALSE;

		if (argv.length < 4) {
			System.err.println("Generator needs 4 arguments: <output directory> <start> <stop> <segments>");
			return 1;
		}
		// parse arguments
		directory=argv[0];
		from = argv[1].toLowerCase();
		to = argv[2].toLowerCase();
		segments = Integer.valueOf(argv[3]);

		writers = new SequenceFile.Writer[segments];
		current = DNStoInt(from);
		stop = DNStoInt(to);
		FALSE = new BooleanWritable(false);

		active = 0;

		for(int i=0;i<writers.length;i++) {
			Path name;
			name = new Path(directory+"/"+from+"_"+to+"_"+i);
			writers[i] = SequenceFile.createWriter(getConf(), SequenceFile.Writer.file(name),
					SequenceFile.Writer.keyClass(Text.class), SequenceFile.Writer.valueClass(BooleanWritable.class),
					SequenceFile.Writer.compression(CompressionType.BLOCK));
		}

		do {
			// write current key to active segment
			writers[active++].append(new Text(intToDNS(current)), FALSE);
			if (active >= writers.length)
				active = 0;
			current = nextDNS(current);
		} while(!Arrays.equals(current,stop));

		// write last record
		writers[0].append(new Text(intToDNS(current)), FALSE);

		for(int i=0;i<writers.length;i++) {
			writers[i].close();
		}

		return 0;
	}

	/**
	 * Return next dns name in row
	 */
	public static final int[] nextDNS(int[] old) {
		boolean ok = false;
		for (int i=old.length-1;i>=0;i--) {
			if(++old[i] < validchars.length) {
				ok = true;
				break;
			}
			else
				old[i] = 0;
		}
		if ( ok == false ) {
			return new int[old.length + 1];
		} else
			return old;
	}

	/**
	 * Convert DNS name to int array
	 */
	public static final int[] DNStoInt(String dns) {
		dns=dns.toLowerCase();
		int[] rc;
		rc = new int[dns.length()];
		String validator = new String(validchars);

		for(int i=0;i<rc.length;i++) {
			if ( ! validator.contains(dns.charAt(i)+""))
				throw new IllegalArgumentException("invalid character in dns name: "+dns.charAt(i));
			rc[i] = validator.indexOf(dns.charAt(i));
		}
		return rc;
	}

	/**
	 * Convert Int back to DNS name
	 */
	public static final String intToDNS(int[] dns) {
		StringBuilder rc = new StringBuilder(dns.length);

		for(int i=0;i<dns.length;i++) {
			rc.append(validchars[dns[i]]);
		}
		return rc.toString();
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
        // Let ToolRunner handle generic command-line options
    	System.exit(ToolRunner.run(new Configuration(), new Generator(), args));
	}
}
