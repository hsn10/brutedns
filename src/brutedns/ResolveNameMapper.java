package brutedns;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.hadoop.configuration.ConfigurationFactoryBean;

import brutedns.microresolv.DNSChecker;
import brutedns.microresolv.DNSQuery;
import brutedns.microresolv.IllegalDNSQuery;

/**
 * Check if nameserver for CZ domain exists
 */
public class ResolveNameMapper extends Mapper<Text, BooleanWritable, Text, BooleanWritable> {

	private static final Logger LOG  = LoggerFactory.getLogger(ResolveNameMapper.class);

	private DNSChecker c;
	private BooleanWritable TRUE;
	private BooleanWritable FALSE;

	@Override
	public void map(Text key, BooleanWritable value, Context context) throws InterruptedException, IOException {
		try {
			boolean rc = value.get();

			if ( rc == false ) {
				rc = c.exists(new DNSQuery(key.toString()+".cz", (short) 2));
				if ( rc ) {
					context.write(key, TRUE);
					context.getCounter(Counters.FOUND).increment(1);
				} else
					context.getCounter(Counters.NOT_FOUND).increment(1);
			}
			else {
				/* already known domain */
				context.getCounter(Counters.KNOWN).increment(1);
				context.write(key, TRUE);
			}
		}
		catch (IllegalDNSQuery e) {
			// retry this name later
			context.write(key, FALSE);
			context.getCounter(Counters.QUERY_FAILED).increment(1);
		}
		catch (TimeoutException e) {
			// retry this name later
			context.write(key, FALSE);
			context.getCounter(Counters.TIMEOUT).increment(1);
		}
	}

	@Override
	protected void cleanup(Context context)
			throws IOException, InterruptedException {
		c.close();
	}

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		LOG.debug("mapper started");
		c = new DNSChecker("127.0.0.1");
		TRUE = new BooleanWritable(true);
		FALSE = new BooleanWritable(false);
		/* register spring resource hdfs handler */
		ConfigurationFactoryBean cfg = new ConfigurationFactoryBean();
		cfg.setRegisterUrlHandler(true);
		try {
			cfg.afterPropertiesSet();
		} catch (Exception e) { /* ignore */ }
	}
}
