package brutedns.microresolv;

/**
 * Represents DNS name
 * Normally used for converting DNS names from/to wire format
 *
 * @author Radim Kolar
 */
public class Name {

	protected String name;

	/**
	 * Creates Name object with supplied name.
	 *
	 * name must be valid DNS name, no case conversion is done.
	 *
	 * @throws IllegalRecordFormat if name contains bad characters for DNS name
	 * @param name DNS name
	 */
	public Name(String name) throws IllegalRecordFormat {
		if ( name == null )
			throw new NullPointerException("You cant use null name in this constructor");
		if ( isNameValid(name) == false )
			throw new IllegalRecordFormat(name+" is not valid DNS name");
		this.name = name;
	}

	/**
	 * Creates Name object from DNS wire name representation
	 *
	 * DNS name compression is not supported
	 *
	 * @param ibuf	byte buffer containing DNS name
	 * @param start	start of DNS name (inclusive)
	 * @param end	end of DNS name (exclusive)
	 */
	public Name(byte[] ibuf, int start, int end) throws IllegalDNSQuery
	{
		if ( start < 0 || start >= ibuf.length )
			throw new IllegalArgumentException("start offset out of range");
		if ( end < 0 || end > ibuf.length )
			throw new IllegalArgumentException("end offset out of range");

		StringBuilder name=new StringBuilder ( 64 );
		int lsize = ibuf [start];
		int pos = start + 1;
		while ( lsize != 0) {
			if ( lsize < 0)
				throw new IllegalDNSQuery("Label size is negative, compression is not supported");
			if (pos + lsize + 1 > end )
				throw new IllegalDNSQuery("End of packet reached during reading label");
			if ( pos != start + 1 )
				name.append('.');
			name.append(new String(ibuf,pos,lsize));
			pos += lsize;
			lsize = ibuf [ pos ];
			pos++;
		}
		this.name = name.toString();
		if ( isNameValid(this.name) == false )
			throw new IllegalDNSQuery(name+" is not valid DNS name");
	}

	/**
	 * Checks if string is valid DNS name.
	 *
	 * All domain names must follow the same character rules:
	 *    You can use letters (abc),
	 *    numbers (123)
	 *    and dashes/hyphens (---).
	 *    Spaces are not allowed and the domain can't begin or end with a dash.
	 *
	 * @param name DNS name to be checked
	 * @return true for valid DNS names, false otherwise
	 */
	public static boolean isNameValid(String name) {
		char c;
		boolean dot;

		// disallow empty strings as valid DNS names
		if (name.length() == 0)
			return false;

		// check allowed characters in names and double dots

		dot = true;
		for (int i = 0; i < name.length(); i++ )
		{
			c = name.charAt(i);

			if ( dot == true)
			{
				// names can not start with dot
				if ( c == '.' )
					return false;
			}
			if ( c == '.' )
			{
				dot = true;
			} else
			{
				dot = false;
				if ( c == '-' )
					continue;
				if ( c < '0' || c > 'z' )
					return false;
				if ( c > '9' && c < 'A' )
					return false;
				if ( c > 'Z' && c < 'a' )
					return false;
			}
		}
		// check for dash or dot at end of name
		if ( dot == true )
			return false;
		return true;
	}

	/**
	 * Returns name in label packed DNS wire format.
	 *
	 * @return label packed uncompressed DNS name
	 */
	public byte[] toWire() {
		byte[] result;

		/* test cases for determining size of result array */
		// 0 -> 1
		// 1 -> 3 a
		// 2 -> 4 aa
		// 3 -> 5 bbb
		// 3 -> 5 b.b

		result = new byte [ 2 + name.length() ];

		int pos = 0;
		int opos = 0;
		int dot;
		int lsize;

		byte[] bname = name.getBytes();

		while ( pos < bname.length )
		{
			dot = name.indexOf('.', pos );
			if ( dot == -1 )
				dot = bname.length;

			lsize = dot - pos;
			result [ opos++ ] = (byte) lsize;
			System.arraycopy(bname, pos, result, opos, lsize);
			opos += lsize;
			pos = dot + 1;
		}

		/* add ending terminator */
		result [ opos ] = 0;
		return result;
	}

	/**
	 * Return DNS name held in this object
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return length of this name in wire representation
	 */
	public int length() {
		return name.length() + 2;
	}

	/**
	 * Convert name back to String
	 *
	 * @return DNS name in String format
	 */
	@Override
	public String toString() {
		return name;
	}
}
