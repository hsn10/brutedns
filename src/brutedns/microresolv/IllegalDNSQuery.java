package brutedns.microresolv;

/**
 * This exception thrown when parsing DNS Query fails.
 *
 * @author Radim Kolar
 */
public class IllegalDNSQuery extends Exception {

	static final long serialVersionUID = 5824443034581930431L;

	public IllegalDNSQuery(String reason)
	{
		super(reason);
	}
}