package brutedns.microresolv;

/**
 * This exception is thrown when illegal input format
 * of DNS record is detected.
 *
 * @author Radim Kolar
 */
public class IllegalRecordFormat extends Exception {

	static final long serialVersionUID = 9143862278136667388L;

	public IllegalRecordFormat(String reason) {
		super(reason);
	}
}
