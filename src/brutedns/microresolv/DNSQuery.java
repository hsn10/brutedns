package brutedns.microresolv;

/**
 * Class for parsing DNS Queries.
 *
 * @author Radim Kolar
 */
public class DNSQuery {

	/**
	 * domain name to query.
	 */
	protected String name;

	/**
	 * type of the query.
	 *
	 * The values for this field include all codes valid for a
	 * TYPE field, together with some more general codes which
	 * can match more than one type of RR.
	 */
	protected short type;

	/**
	 * Size of DNS packet header.
	 */
	public final static int HDR_SIZE = 12;

	/**
	 * Minimum number of input bytes needed for parsing
	 * smallest possible valid DNS query.
	 */
	public final static int MIN_SIZE = HDR_SIZE+3+4;

	/**
	 * For simplicity we support just Internet queries
	 * (Class IN).
	 */
	public final static short CLASS_IN = 1;

	/**
     * Create DNSQuery object from buffer containing DNS query.
	 *
	 * @param ibuf		buffer holding DNS packet data
	 * @param ilength	length of input data
	 */
 	public DNSQuery(byte ibuf[], int ilength) throws IllegalDNSQuery {
 		if ( ibuf == null )
 			throw new NullPointerException("ibuf is null");
		if ( ilength < MIN_SIZE )
			throw new IllegalDNSQuery("Not enough input data");
		if ( ibuf.length < ilength )
			throw new IllegalArgumentException("Length of input data is greater than buffer size");
		/* check header flags */
		if ( ( ibuf[2] & 0xFA) != 0x00 )
			throw new IllegalDNSQuery("Illegal query flags in header");
		/* check number of queries in packet */
		/* we are allowing max 127 queries in packet, but
		 * only first is processed. */
		if ( (ibuf[4] != 0) || (ibuf[5] < 1) )
			throw new IllegalDNSQuery("Too much queries in packet");
		/* 3 unsigned 16bit counters follows:
		       number of answers, authority and and additional records.
		       We do not check them but in correct query packet all 3 should
		       be set to zero.
		 */

		Name n;
		int pos;
		/* decode name */
		n = new Name (ibuf, HDR_SIZE, ilength);
		pos = HDR_SIZE + n.length();
		if ( pos + 4 > ilength )
			throw new IllegalDNSQuery("Query truncated");
		/* 2 16bit unsigned values follows:
		   query type and query class
		 */
		type = (short) ( ibuf [ pos ] << 8 | ibuf [ pos + 1] );
		pos += 2;
		if ( ibuf [pos] != 00 || ibuf [pos+1] != CLASS_IN)
			throw new IllegalDNSQuery("Non IN Class queries are not supported");
		name = n.name;
	}

 	/**
 	 * Create DNSQuery object from use supplied data.
 	 * @param name query name
 	 * @param type query type, can not be zero
 	 * @throws IllegalDNSQuery
 	 */
 	public DNSQuery(String name, short type) throws IllegalDNSQuery {
 		if ( name == null )
 			throw new NullPointerException("name is null");
 		if ( type == 0)
 			throw new IllegalDNSQuery("Query type must be nonzero");

 		this.name = name;
 		this.type = type;
 	}

 	/**
 	 * Converts DNSQuery to wire format.
 	 *
 	 * @return DNSQuery in wire format
 	 */
 	public byte[] toWire() {
 		byte[] result;
 		byte[] ntmp;
 		Name n = null;
 		int pos;

 		try {
			n = new Name(name);
		} catch (IllegalRecordFormat e) {
			// should never happen because we check name when we
			// are creating DNSQuery
			throw new RuntimeException(e);
		}
 		ntmp = n.toWire();

 		result = new byte [ntmp.length + 4];
 		System.arraycopy(ntmp, 0, result, 0, ntmp.length);

 		pos = ntmp.length;
 		result [ pos++ ] = (byte) (type >>> 8);
 		result [ pos++ ] = (byte) (type & 0xFF);
 		result [ pos + 1 ] = CLASS_IN;
 		return result;
 	}

	/**
	 * Convert DNSQuery to string suitable for debug prints
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Query "+name+" type "+type;
	}

	/**
	 * Return domain name what we are querying
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return type what we are querying
	 */
	public short getType() {
		return type;
	}
 }