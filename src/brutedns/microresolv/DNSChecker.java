package brutedns.microresolv;

import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DNSChecker implements Closeable {
	private final static Logger LOG = LoggerFactory.getLogger(DNSChecker.class);

	DatagramSocket s;
	short id;
	Random rnd;
	int retries;
	int timeout;
	InetAddress server;

	public DNSChecker(String ns) throws SocketException, UnknownHostException {
		rnd = new Random();
		s = new DatagramSocket();
		retries = 1;
		timeout = 300;
		s.setSoTimeout(timeout);
		server = InetAddress.getByName(ns);
	}

	/* check if DNS name exists */
	public boolean exists(DNSQuery q) throws TimeoutException, IllegalDNSQuery, InterruptedException {
		byte[] payload;
		DatagramPacket pkt;

		// generate new query id
		id = (short)rnd.nextInt(Short.MAX_VALUE+1);

		nexttry:for(int i=0;i<=retries;i++) {
			payload = makeDNSPacket(q);
			pkt = new DatagramPacket(payload, payload.length);
			pkt.setPort(53);
			pkt.setAddress(server);

			try {
				s.send(pkt);
			}
			catch (IOException e) { Thread.sleep(timeout);
			continue;
			}

			while(true) {
				try {
					s.receive(pkt);
				}
				catch (SocketTimeoutException e) { continue nexttry; }
				catch (IOException e) { continue; }

				payload = pkt.getData();

				// check answer flag
				if ( (payload[2] & 0x80) == 0 )
					continue;
				// check ID in response
				if ( id != (short) ( (payload [ 0 ] << 8) | ( payload [ 1 ] & 0xFF ) ))
					continue;
				// check response code. 3 = NX DOMAIN
				switch(payload [3] & 0x0f) {
				case 3:
					// NX-DOMAIN
					return false;
				case 0:
					// No error
					return true;
				default:
					throw new IllegalDNSQuery("bad reply from server");
				}
			}
		}

		throw new TimeoutException();
	}

	private byte[] makeDNSPacket(DNSQuery q) {
		byte[] query,header;
		byte[] rc;

		header = makeDNSHeader();
		query = q.toWire();

		rc = new byte[query.length + DNSQuery.HDR_SIZE];
		System.arraycopy(header, 0, rc, 0, DNSQuery.HDR_SIZE);
		System.arraycopy(query, 0, rc, DNSQuery.HDR_SIZE, query.length);

		return rc;
	}

	private byte[] makeDNSHeader() {
		byte[] rc;

		rc = new byte [DNSQuery.HDR_SIZE];
		// set query id
		rc[0] = (byte) (id >> 8);
		rc[1] = (byte) ( id & 0xFF );
		// set recursion desired flag
		rc[2] = 1;
		// one query in packet
		rc[5] = 1;

		return rc;
	}

	public static void main(String argv[]) throws Exception {
		DNSChecker c;
		LOG.info("Performing quick DNS Check against 10.0.0.7");
		c = new DNSChecker("10.0.0.7");
		System.out.println(c.exists(new DNSQuery("seznam.cz", (short)2)));
		System.out.println(c.exists(new DNSQuery("kurvaxx2x.cz", (short)2)));
		c.close();
	}

	@Override
	public void close() throws IOException {
		s.close();
	}
}
