package brutedns;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.springframework.data.hadoop.configuration.ConfigurationFactoryBean;

/**
 * Hadoop driver
 */
public class Run extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		ConfigurationFactoryBean cfg = new ConfigurationFactoryBean();
		cfg.afterPropertiesSet();
		
		// Let ToolRunner handle generic command-line options
		System.exit(ToolRunner.run(cfg.getObject(), new Run(), args));
	}

	@Override
	public int run(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println("brutedns.Run <inputdir> <outputdir>");
			return -1;
		}

		Job job;

		job = Job.getInstance(getConf());
		job.setJobName("brutedns");
		job.setJarByClass(ResolveNameMapper.class);
		System.out.println("Job jar set to: "+job.getJar());

		job.setMapperClass(ResolveNameMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(BooleanWritable.class);

		job.setNumReduceTasks(0);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(BooleanWritable.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		FileInputFormat.setInputPaths(job, args[0]);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
		job.submit();
		return job.waitForCompletion(true) ? 0 : 1;
	}
}
