package brutedns;

public enum Counters {
	  KNOWN,
	  TIMEOUT,
	  QUERY_FAILED,
	  FOUND,
	  NOT_FOUND
}
